# Summary:

```
# After booting from the installation medium:
sudo -s                                    # work as root
ping -c2 nixos.org                         # check connectivity
cat /proc/partitions; ls -al /dev/disk/*   # figure out which disk to install to

# Partition that disk, create filesystems and mount
parted --script "${DISK}" -- \
        mklabel gpt \
        mkpart esp fat32 1MiB 1GiB \
        mkpart primary 1GiB 100% \
        set 1 boot on
mkfs.fat -F 32 -n BOOT /dev/sda1
mkfs.ext4 /dev/sda2
mount /dev/sda2 /mnt/
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot/

# Configure the new system
nixos-generate-config --root /mnt/
nix-env -i curl
#tt
cd /mnt/etc/nixos
curl https://dechnik.net/configuration.nix > configuration.nix
vi configuration.nix                      # edit according to comments and preferences
nixos-install
reboot                                    # remember removing install medium
```
