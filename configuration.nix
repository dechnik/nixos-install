# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let

  main_user = "lukasz";
  hostname = "nixos";

in {
  time.timeZone = "Europe/Warsaw";
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.opengl.driSupport32Bit = true;

  boot = {
    tmpOnTmpfs = true;
    kernel.sysctl."vm.swappiness" = 1;
    #loader.grub = {
    #  enable = true;
    #  version = 2;
    #  device = "nodev"; # or "nodev" for efi only
    #  efiSupport = true;
    #  #efiInstallAsRemovable = true;
    #};
    loader.systemd-boot = {
      enable = true;
    };
    loader.efi = {
      #efiSysMountPoint = "/boot/efi";
      canTouchEfiVariables = true;
    };
  };

  networking = {
    hostName = hostname; # Define your hostname.
    useDHCP = false;
    networkmanager.enable = true;
    #interfaces."${physical_interface}".useDHCP = true;
    #interfaces."${wifi_interface}".useDHCP = true;
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.
    # proxy.default = "http://user:password@proxy:port/";
    # proxy.noProxy = "127.0.0.1,localhost,internal.domain";
    # Open ports in the firewall.
    firewall.allowedTCPPorts = [ 22 ];
    # firewall.allowedUDPPorts = [ ... ];
    # Or disable the firewall altogether.
    firewall.enable = true;
  };

  imports = [ ./hardware-configuration.nix ]
      ++ (if builtins.pathExists ./cachix.nix then [ ./cachix.nix ] else []);

  nixpkgs = {
    config.allowUnfree = true;
    config.packageOverrides = pkgs: {
      nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
        inherit pkgs;
      };
    };
  };

  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
        LC_CTYPE="pl_PL.UTF-8";
        LC_NUMERIC="pl_PL.UTF-8";
        LC_TIME="pl_PL.UTF-8";
        LC_MONETARY="pl_PL.UTF-8";
        LC_PAPER="pl_PL.UTF-8";
        LC_NAME="pl_PL.UTF-8";
        LC_ADDRESS="pl_PL.UTF-8";
        LC_TELEPHONE="pl_PL.UTF-8";
        LC_MEASUREMENT="pl_PL.UTF-8";
        LC_IDENTIFICATION="pl_PL.UTF-8";
    };
  };
  console = {
    font = "Lat2-Terminus16";
    keyMap = "pl";
  };

  users.users."${main_user}" = {
    isNormalUser = true;
    extraGroups = [ "wheel" "audio" "networkmanager" "wireshark" "dialout" "disk" "video" "docker" "networkmanager" ];
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC0KQTUimUPPQdHmW/dkQZRTr/EJy/Nxpmz2BoUdOHMeeFx/xWqWvQQQOPCRrbAf6vCgp9xi2e2L9ZTIr6N+XJS0Uvq3redkBtQ2ZptMVo+RjH5TwYmuw5l43Ql4XqlJTfgC+IqjzPArv/4n4huALGCg8mRSZvNsdwPpTjLlLeuFKDg+Hc4nBK5+mBtOC2W3puUA1aOXozqsnipVVwES9/sSdd6gh9CzYpMtEIubk7TbrIRPEzQeCTuwXID0vKtY+rAGUCQfPQLzErxw/49jbi2jXGUCqOYlMaLg9vU5eB2CzWWfUe6tqS2KfXt5lWmIVFG88GvshNqXpboa13qWw6oxCV4d+/UZOQjb8WgOvn5toCi9WJsUenSQtpIeSX6eQirXyQtVZEPE3dsB4J8NPA/wKiXwSxkWZ8AhK+ejJLfJT4NvFZOEFbgdmbsD77OrfgIQE/d9hsye1x54f4frzkTeTW2CMHJvo5YUnVEz/FLrfikPxaKgfaRCsAVjEw/tIZyYqZcY7CphFWgJzmU3W4vKonx2FD5vjhUPrS9EMxxOEiH8Dsi1wcJxUQHKdWrxC0DElN3pPpEZDWrvWXaAxKXdNWLhqAFUWTNpGlsSuO/ToizLnDL2ZctHBvwMPjTBrNj6IDf/IGsUI5OEg6iZY0zHXmkbNCxqz16qJhZXvEdKQ==" ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment = {
    systemPackages = with pkgs; [
      (neovim.override {viAlias = true; vimAlias = true;})
      wget
      git
      curl
      gnupg
      htop
      openssl
      rsync
      w3m
      inetutils
      dnsutils
      nur.repos.instantos.instantnix
    ];
    variables = {
      EDITOR = "nvim";
    };
    homeBinInPath = true;
};

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services = {
    openssh = {
      enable = true;
    };
    qemuGuest = {
      enable = true;
    };
    udev = {
      extraRules = ''
        ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", MODE="0666", RUN+="${pkgs.coreutils}/bin/chmod a+w /sys/class/backlight/%k/brightness"
      '';
    };
    xserver = {
      layout = "us";
      xkbVariant = "intl";
      libinput.enable = true;
      autorun = true;
      enable = true;
      displayManager = {
        startx.enable = true;
        gdm.enable = false;
        sddm.enable = false;
      };
      desktopManager = {
        gnome3.enable = false;
        plasma5.enable = false;
        xterm.enable = false;
      };
    };
  };

  fonts = {
    fonts = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      jetbrains-mono
      meslo-lg
      libertine
      liberation_ttf
      fira-code
      fira-code-symbols
      dina-font
    ];
  };

  nix = {
    allowedUsers = [ "${main_user}" ];
    autoOptimiseStore = true;
    gc = {
      automatic = true;
      options = "--delete-older-than 14d";
    };
  };

  #system.stateVersion = "20.09"; # Did you read the comment?

}
